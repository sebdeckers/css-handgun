# Firearm Emoji Censorship

Reclaim the Apple-censored pistol emoji with pure CSS.

Demo:
https://codepen.io/sebdeckers/pen/gOpLmaz

Context:
https://blog.emojipedia.org/apple-and-the-gun-emoji/
